package com.company;
import java.util.ArrayList;
import java.util.List;

//Using the Linked List class, create a function that removes the first element.
//Using the Linked List class, create a size variable that determines how many elements are present in your Linked List.
// if it's an in-place counter, meaning it automatically updates the count whenever you remove or add an element.
//Using the LinkedList class, create a contains() and indexOf()  function similar to our ArrayList.
//Bonus:
//From our implementation of the Singly Linked List class, each node only has a reference to the next element.
// Create an implementation of a Doubly Linked List class wherein the next element also has a reference to the previous element.
// Refer to the illustration below.
public class Main
{
    public static void main(String[] args)
    {
        List<Player> playerList = new ArrayList<>();

   /*     playerList.add(new Player(1, "S1NX", 99));
        playerList.add(new Player(2, "WhyteKnight", 98));
        playerList.add(new Player(3, "Karlito", 97));*/

        Player S1NX = new Player(1,"S1NX",99);
        Player WhyteKnight = new Player(2,"WhyteKnight",98);
        Player Karlito = new Player(3,"Karlito",97);



      /*for(Player p : playerList)
      {
        System.out.println(p);
      }
        System.out.println("");*/

      //System.out.println(playerList.contains(new Player(1, "S1NX", 99)));

      //System.out.println("");

        //System.out.println(playerList.indexOf(new Player(1, "S1NX", 99)));

        PlayerLinkList playerLinkList = new PlayerLinkList();

        playerLinkList.addToFront(S1NX);
        playerLinkList.addToFront(WhyteKnight);
        playerLinkList.addToFront(Karlito);

        System.out.println("");
        System.out.println("Linklist:");
        playerLinkList.printList();
        System.out.println("");
        System.out.println("Delete type:");
        playerLinkList.delete();
        System.out.println("");
        System.out.println("How many elements left:");
        System.out.print(playerLinkList.getCurrentSize());
        System.out.println("");
        System.out.println("");
        System.out.println("Contains:");
        System.out.println(playerLinkList.contain(WhyteKnight));
        System.out.println("");
        System.out.println("IndexOf:");
        System.out.println(playerLinkList.indexOf(S1NX));
        System.out.println(playerLinkList.indexOf(WhyteKnight));
        System.out.println(playerLinkList.indexOf(Karlito));



    }

}
