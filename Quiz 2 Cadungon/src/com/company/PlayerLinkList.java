package com.company;

import java.util.Objects;

public class PlayerLinkList
{
    private  PlayerNode head;
    private int  currentSize = 0;


    public void addToFront(Player player)
    {
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;

        currentSize++;
    }
    public int getCurrentSize()
    {
        return currentSize;
    }

    public void printList()
    {
        PlayerNode current = head;
        System.out.print("HEAD -> ");

        while (current != null)
        {
            System.out.print(current);
            System.out.print(" -> ");
            current = current.getNextPlayer();
        }

        System.out.println("NULL");

    }


    public void delete()
    {
        PlayerNode current = head;

        PlayerNode  temporary = head;

        temporary = current;

        current = current.getNextPlayer();

        temporary = null ;
        System.out.print("HEAD -> ");

        while (current != null)
        {
            System.out.print(current);
            System.out.print(" -> ");
            current = current.getNextPlayer();
        }

        System.out.println("NULL");

        currentSize--;
    }

    public  boolean contain(Player player)
    {
        PlayerNode tempHead = head;

        while (tempHead != null)
        {
            if (tempHead.getPlayer() == player)
            {
                return true;
            }
            tempHead = tempHead.getNextPlayer();

        }
        return  false;
    }

    public int indexOf(Player player)
    {
        int x = 0;
        PlayerNode tempHead = head;
        if(!contain(player)) // if (!contain(player))
        {
            return -1;
        }
        while  (tempHead != null)
        {
            if (tempHead.getPlayer() == player)
            {
                return player.getId();
            }
            tempHead = tempHead.getNextPlayer();
            x++;
        }
        return 0;
    }


}
