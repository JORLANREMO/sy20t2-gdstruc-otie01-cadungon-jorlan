package com.company;


import java.util.ArrayList;
import java.util.Scanner;

/*Parts of the Project

    Turns

    The program gives commands every turn. The commands are given at random.
    Possible commands are:
    Draw x cards (x can be a random value from 1 to 5)
    Discard x cards
    Get x cards from the discarded pile
    After each round, the program must display the following info:
    List of cards that the player is currently holding
    Number of remaining cards in the player deck
    Number of cards in the discarded pile

    Player Deck

    Contains 30 cards
    Give each card a name. Names can be duplicated.


    Discarded Pile

    This contains all the cards discarded by the player.

    Notes

    The player deck and discarded pile should work as a STACK. The next card to be drawn must be the top of the deck.
    You may create the following classes:
    Card.java
    CardStack.java
    You may use an array or a linked list for your Stack class.
    The program ends when the player deck is emptied.*/
public class Main
{

    public static void Draw(int cards, ArrayList<Card> deck)
    {
        for (int i = 0; i < cards; i++)
            deck.add(new Card() );
    }

    public static void main(String[] args)
    {
        ArrayList<Card> playerDeck = new ArrayList<Card>();
        ArrayList<String> stackDeck = new ArrayList<String>();
        Scanner input;
        int ending = 0; // this one can also be boolean
        Card headCard;
        String currentElement;
        int choice;


        gameRemove:
        while (true) // continues on the console
        {
            playerDeck.clear();
            headCard = new Card();
            currentElement = headCard.element;
            System.out.println("Giving out Decks");

            Draw(30,playerDeck);


            for(Boolean turn = true; ending == 0; turn^= true)
            {
                choice = 0;
                System.out.println("The head card is: "+ headCard.getFace());
                System.out.println("Discard pile:" + stackDeck); // still needs fix
                System.out.println("Your Cards are:");// also needs fix

                if (turn)
                {

                    stackDeck.add(headCard.getFace());

                    for(int i = 0; i < playerDeck.size(); i++)
                    {
                        System.out.println(String.valueOf(i + 1) + " - "+ ((Card) playerDeck.get(i) ).getFace());


                    }
                }
                do
                {
                    System.out.print("Please input the number of your choice: ");
                    input = new Scanner(System.in);

                } while (!input.hasNextInt());  // input on what you are going to pick from the deck from above

                choice = input.nextInt() - 1; //

                if (choice == playerDeck.size() )
                    Draw(1, playerDeck);
                else if (choice == playerDeck.size() + 1)
                    break gameRemove;
                {
                    headCard = (Card) playerDeck.get(choice);
                    playerDeck.remove(choice);
                    currentElement = headCard.element;


                }

            }

            if(playerDeck.size() == 0)
            {
                ending = 1;

                if(ending == 1) // still work in progress but a place holder
                {
                    System.out.println("You have emptied your cards");
                }
            }
        }

    }
}
