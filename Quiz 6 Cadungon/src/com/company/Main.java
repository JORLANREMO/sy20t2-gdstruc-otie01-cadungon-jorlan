package com.company;

public class Main {
/*
    Node getMin() - gets the node with the least value in the tree
    Node getMax() - gets the node with the maximum value in the tree
    void traverseInOrderDescending() - traverses the tree using the in-order method
    but in descending order.*/



    public static void main(String[] args)
    {
        Tree tree = new Tree();

        tree.insert(20);
        tree.insert(16);
        tree.insert(10);
        tree.insert(27);
        tree.insert(14);
        tree.insert(74);
        tree.insert(-8);
        tree.insert(33);
        tree.insert(-11);

        System.out.println(" ");
        tree.getMax();
        tree.getMin();
        System.out.println(" ");
        System.out.println("Ascending Order Tree");
        tree.traverseInOrder();
        System.out.println(" ");
        System.out.println(tree.get(100));
        System.out.println(" ");
        System.out.println("Descending Order Tree");
        tree.traverseInOrderDescending();
        System.out.println(" ");


    }
}
