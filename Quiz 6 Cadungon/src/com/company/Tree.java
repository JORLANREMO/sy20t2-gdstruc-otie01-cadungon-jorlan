package com.company;

public class Tree
{
    private  Node root;

    public Node get(int value)
    {
        if (root != null)
        {
            return root.get(value);
        }
        return null;
    }

    public  void insert (int value)
    {
        if (root == null)
        {
             root = new Node(value);
        }
        else{
            root.insert(value);
        }
    }

    public void traverseInOrder()
    {
        if(root != null)
        {
            root.traverseInOrder();
        }
    }


    public void traverseInOrderDescending()
    {
        if (root != null)
        {
            root.traverseInOrderDescending();
        }
    }

    public Node getMax()
    {
        if(root != null)
        {
            root.getMax();
        }
        return root;
    }

    public Node getMin()
    {
        if(root != null)
        {
            root.getMin();
        }
        return  root;
    }

}
