package com.company;

public class Main {

    public static void main(String[] args)
    {
        // 40pts: Modify the BubbleSort and SelectionSort to sort arrays in descending order
        // 60pts: Modify the Selection Sort to look for the smallest value first and put it at the end
        // instead of looking for the largest and putting it in the beginning.


        int[] numbers = new int [10];

        numbers[0] = 12;
        numbers[1] = -1;
        numbers[2] = 19;
        numbers[3] = 2;
        numbers[4] = 42;
        numbers[5] = 27;
        numbers[6] = 69;
        numbers[7] = 87;
        numbers[8] = 36;
        numbers[9] = 33;

        System.out.println("The default order is:");
        printArrayElements(numbers);

        bubbleSort(numbers);
        System.out.println("\nThe descending bubble sort order is:");
        printArrayElements(numbers);

        selectionSort(numbers);
        System.out.println("\nThe descending selection sort order is:");
        printArrayElements(numbers);
    }
    private static void bubbleSort (int[] x) // this function will try to swap the elements
    {
        System.out.println("");// gives cleanliness to the build
        for(int currentValueIndex = x.length - 1;currentValueIndex > 0;--currentValueIndex) // the "-1" is use in order for it not to throw out of bound
        {
            for (int i = 0;i < currentValueIndex;i++)
            {
                if(x[i] < x[i + 1]) // this allows the descending order of sorting
                {
                    int temporary = x[i];
                    x[i] = x[i + 1];
                    x[i + 1] = temporary; // 36 - 38 line this code allows the sorting

                }
            }
        }
    }

    private static void printArrayElements (int[] x) // this function prints the arrays
    {
        for (int j : x)
        {
            System.out.print(j + " ");
        }
    }

    private static void selectionSort(int[] x) // this this function will try to swap the elements
    {
        System.out.println(""); // gives cleanliness to the build
        for(int currentValueIndex = x.length - 1;currentValueIndex > 0;currentValueIndex--)
        {
            int bigIndex = 0; // checks the biggest index on the array

            for (int i = 1;currentValueIndex >= i; i++)
            {
                if(x[i] < x[bigIndex]) // will check each element if its bigger or not and will do it in descending order
                {
                    bigIndex = i; // finds the biggest index in the array
                }
            }
            int temporary = x[currentValueIndex];
            x[currentValueIndex] = x[bigIndex];
            x[bigIndex] = temporary; // 76 - 78 line this code allows the sorting
        }

    }

}

