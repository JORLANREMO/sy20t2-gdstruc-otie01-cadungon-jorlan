package com.company;

/*Every turn, x players will queue for matchmaking (x = rand () 1 to 7). Pressing enter ends the turn.
  A game can be started when at least 5 players are in the queue.
  When a game starts, pop the first 5 players from the queue. Done
  The program terminates when 10 games have been successfully made.*/
import java.util.EmptyStackException;

public class ArrayQueue
{
    private Player[] queue;
    private int front;
    private int end;
    private int playerNumber;

    public ArrayQueue(int capacity)
    {
        queue = new Player[capacity];
    }
    public void add(Player player)
    {
        // array resize when full
        if (end == queue.length)
        {
            Player[] newArray = new Player[queue.length * 2];
            System.arraycopy(queue, 0, newArray, 0, queue.length);
            queue = newArray;

        }

        queue[end] = player;
        end++;

    }
    public Player remove()
    {
        if (size() == 0)
        {
            throw new EmptyStackException();
        }

        Player removePlayer = queue[front];
        queue[front] = null;
        front++;
        if( size() == 0) // puts back the size to zero again
        {
            front = 0;
            end = 0;
        }

        return removePlayer;
    }
    public Player peek()
    {

        if (size() == 0)
        {
            throw new EmptyStackException();
        }

        return queue[front];
    }
    public int size()
    {
        return end - front;
    }

    public void printQueue()
    {
        for (int i = front; i < end; i++)
        {
            System.out.println(queue[i]);
        }

    }




}
