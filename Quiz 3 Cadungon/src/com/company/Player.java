package com.company;

import java.util.Objects;

public class Player
{
    private int id;
    private int level;

    public Player(int id, int level)
    {
        this.id = id;
        this.level = level;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getLevel()
    {
        return level;
    }

    public void setLevel(int level)
    {
        this.level = level;
    }

    @Override
    public String toString()
    {
        return "Player" + "{" + id  + " " + level + '}';
    }
    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return id == player.id && level == player.level;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, level);
    }
}
