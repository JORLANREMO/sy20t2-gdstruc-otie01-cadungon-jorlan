package com.company;



public class Main {

    public static void main(String[] args)
    {

        Player O = new Player(10,"O",1);
        Player S1nx = new Player(118, "S1nx", 69);
        Player Daigo = new Player(777, "Daigo", 100);
        Player ElderWood = new Player(500, "ElderWood", 101);
        Player Pepito = new Player(117, "Pepito", 50);
        Player Karlito = new Player(318, "Karlito", 79);
        Player ELlo = new Player(123,"ELlo", 45);

        HashTable hashTable = new HashTable();

        hashTable.put(O.getName(),O);
        hashTable.put(S1nx.getName(),S1nx);
        hashTable.put(Daigo.getName(),Daigo);
        hashTable.put(ElderWood.getName(), ElderWood);
        hashTable.put(Pepito.getName(), Pepito);
        hashTable.put(Karlito.getName(), Karlito);
        hashTable.put(ELlo.getName(), ELlo);

        hashTable.printHashTable();
        System.out.println(hashTable.get("ELlo"));
        hashTable.delete(118);

        System.out.println(" ");
        hashTable.printHashTable();

    }
}
