package com.company;

import java.util.LinkedList;


public class HashTable
{
    private StoredPlayer[] hashTable;
    //private LinkedList<Player> hashTable;

    public HashTable()
    {
        hashTable = new StoredPlayer[10];//accommodates 10 elements at the very start
    }

    // hashing the key and the key will be using will be the player's username
    private int hashKey(String key)// map the key into a value to 0 to 10
    {
        return key.length() % hashTable.length; // not the best method
        // key.length uses the length of the player username
        // example if character is 4 characters = 4 % 10 = 4
    }

    public void put(String key, Player value)
    {
        int hashedKey = hashKey(key); // hash the key

            // to check the value
        if(isOccupied(hashedKey))
        {
            // do linear probing
            int stoppingIndex = hashedKey;

            if(hashedKey == hashTable.length - 1)
            {
                hashedKey = 0;
            }

            else
            {
                hashedKey++;
            }
            while(isOccupied(hashedKey) && hashedKey != stoppingIndex)
            {
                hashedKey = (hashedKey + 1) % hashTable.length;
            }

        }
        // checks if the same character are on the same element this will output
        if(isOccupied(hashedKey))//hashTable[hashedKey] != null
        {
            System.out.println("An element is already in a position "+ hashedKey);
        }
        // assigns the player to the element
        else
        {
            hashTable[hashedKey] = new StoredPlayer(key, value);
        }

    }
    // function that retrieves an element of player data using the username function
    public Player get(String key)
    {
        int hashedKey = findKey(key); //hashKey(key);
        if (hashedKey == -1)
        {
            return null; // didn't find an element so return to null
        }
        return  hashTable[hashedKey].value;

    }

    private int findKey(String key)
    {
        int hashedKey = hashKey(key);

        // found the right key
        if (hashTable[hashedKey] != null && hashTable[hashedKey].key.equals(key))
        {
            return hashedKey;
        }


        int stoppingIndex = hashedKey;

        if (hashedKey == hashTable.length - 1)
        {
            hashedKey = 0;

        } else {
            hashedKey++;
        }
        while (hashedKey != stoppingIndex
                && hashTable[hashedKey] != null
                && !hashTable[hashedKey].key.equals(key))
        {
            hashedKey = (hashedKey + 1) % hashTable.length;
        }

        if (hashTable[hashedKey] != null
                && hashTable[hashedKey].key.equals(key))
        {
            return hashedKey;
        }
        return -1;
    }
    // determines an element space has already been occupied by an element
    private boolean isOccupied(int index)
    {
        return hashTable[index] != null;
    }

    // prints the element of players
    public void printHashTable()
    {
        for(int i = 0;i < hashTable.length;i++)
        {
            System.out.println("Element (" + i + ") " + hashTable[i]);
        }
    }

    public void delete(int id) // this function will delete the player and making the element null
    {

        int same = 0;
        for(int i =0; hashTable.length > i; i++)
        {
            if(hashTable[i] != null)
            {

                if(id == hashTable[i].value.getId())
                {
                    same = i;
                    hashTable[i] = null;
                }
            }

        }
        for(int x = same + 1;hashTable.length > x;x++)
        {
            hashTable[x -1] = hashTable[x];
        }
            StoredPlayer[] test = new StoredPlayer[hashTable.length - 1];

            for(int x = same + 1;test.length > x;x++)
            {
                test[x] = hashTable[x];
            }
            hashTable = test;
    }

}
