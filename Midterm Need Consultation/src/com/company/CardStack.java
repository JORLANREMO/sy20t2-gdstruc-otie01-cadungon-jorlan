package com.company;

import java.util.EmptyStackException;
import java.util.Scanner;
import java.util.Random;
import java.util.LinkedList;

public class CardStack
{
    private LinkedList<Card> stack;
    private int top;

    public CardStack(int capacity)
    {
        stack = new LinkedList<Card>();
        for(int i = 0; i < capacity; i++){
            stack.addFirst(new Card());
        }
    }

/*    public void push(Card card)
    {
        if(top == stack.size()) // if the stack is full
        {
            Card[] newStack = new Card[2 * stack.size()];
            System.arraycopy(stack,0,newStack,0,stack.size());// when its is copying the array it is copying the index from 0
            stack = newStack;
        } // the whole if statement is to resize the array if the array is full

        stack[top++] = card;// this one pushes the element
    }*/

    /*public Card pop()
    {
        if(isEmpty()) //
        {
            throw new EmptyStackException();

        }
        Card poppedCard = stack[--top];
        stack[top] = null; // to remove garbage values
        return  poppedCard;
    }


    public Card peek()
    {
        if(isEmpty())
        {
            throw new EmptyStackException();
        }
        return stack[top - 1]; // in peek function we don't remove an element, it just for access
        // remember that the top of the stack is always empty
        // the top value should be the next available slot for the stack
    }*/

    public Boolean isEmpty()
    {
        return top == 0;
    }
    public void printStack()
    {
        System.out.println("printing cards stacks:");
        for (int i = top - 1; i >= 0; i--)
        {


            System.out.println(stack.get(i).element);

        }
    }
/*    public Card input()
    {
        Scanner cardInput = new Scanner(System.in);
        int input = cardInput.nextInt();
        System.out.println("Input your card name:");
        System.out.println("You have entered: "+ input);

        return input();
    }*/

    /*public void random()
    {
        Random rand = new Random();
        int sizeCards = 6;
        int random = rand.nextInt(sizeCards);
        System.out.println("Player picked "+ (random)+ " cards");
    }*/

    public Card Draw(){
        Card toDraw = stack.removeFirst();

        return toDraw;
    }

    public void Add(Card card){
        stack.addFirst(card);
    }

    public int getSize()
    {
        return stack.size();
    }

}
