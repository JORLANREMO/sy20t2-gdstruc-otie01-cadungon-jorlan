package com.company;

import java.util.Random;

public class Card
{
    public int number;
    public String element;

    private String top;
    private Random rand;

    public Card(int n, String e)
    {
        number = n;
        element = e;

    }

    public Card()
    {
        rand = new Random();
        number = rand.nextInt(30); // deck of 30 cards

        rand = new Random();
        switch (rand.nextInt(5)) // elements name for cards
        {
            case 0: element = "Fire";
                break;
            case 1: element = "Water";
                break;
            case 2: element = "Earth";
                break;
            case 3: element = "Lightning";
                break;
            case 4: element = "Shadow";
                break;
        }


    }


    public String getFace()
    {
        top = " - ";
        if(element != "none")
        {
            top += this.element + " ";
        }

        switch (this.number)
        {
            default: top += String.valueOf((this.number));
                break;
        }

        top += " - ";
        return top;
    }


    public boolean place(Card x, String y)
    {
        if (this.element == y)
            return true;
        else if (this.number == x.number)
            return  true;

        return false;
    }

}