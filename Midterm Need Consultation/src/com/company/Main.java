package com.company;


import java.util.ArrayList;
import java.util.Scanner;
import java.util.Random;
import java.util.LinkedList;
/*Parts of the Project

    Turns

    The program gives commands every turn. The commands are given at random.
    Possible commands are:
    Draw x cards (x can be a random value from 1 to 5)
    Discard x cards
    Get x cards from the discarded pile
    After each round, the program must display the following info:
    List of cards that the player is currently holding
    Number of remaining cards in the player deck
    Number of cards in the discarded pile

    Player Deck

    Contains 30 cards
    Give each card a name. Names can be duplicated.


    Discarded Pile

    This contains all the cards discarded by the player.

    Notes

    The player deck and discarded pile should work as a STACK. The next card to be drawn must be the top of the deck.
    You may create the following classes:
    Card.java
    CardStack.java
    You may use an array or a linked list for your Stack class.
    The program ends when the player deck is emptied.*/
public class Main
{

    public static void main(String[] args)
    {
        CardStack deck = new CardStack(30);
        CardStack graveyard = new CardStack(0);
        CardStack hands = new CardStack(0);

        while(deck.getSize() > 0 )
        {
            Random rand = new Random();
            int choice = 3;
            int cardRandom = 5;
            int random = rand.nextInt(choice);
            //rand.nextInt(choice) %3;

            if (choice == 0){
                //random
                for(int i = 0; i < rand.nextInt(cardRandom); i++)
                {
                    hands.Add(deck.Draw());
                }
            }
            else if(choice == 2){
                if (graveyard.getSize() <= 0)
                {
                    System.out.println("no cards in graveyard");
                }

                else {
                    if(rand.nextInt(cardRandom) > graveyard.getSize())
                        cardRandom = graveyard.getSize();
                    for(int i = 0; i < rand.nextInt(cardRandom); i++)
                    {
                        hands.Add(graveyard.Draw());
                    }
                }

            }
            else {
                if (hands.getSize() > 0)
                {
                    cardRandom = graveyard.getSize();
                }

                for(int i = 0; i < rand.nextInt(cardRandom); i++)
                {
                    graveyard.Add(hands.Draw());
                }
            }


        }
    }
}